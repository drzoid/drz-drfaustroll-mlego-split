
# WARNING

WARNING: this is a WIP fork of DrFaustroll's m-lego 5x12 split project accomodated for my special needs !

Original repo here: [https://gitlab.com/m-lego/m60_split](https://gitlab.com/m-lego/m60_split)

Many thanks to him for sharing his work !

This is part of a bigger family of ortholinear keyboards in lego see for reference: [https://alin.elena.space/blog/keeblego/](https://alin.elena.space/blog/keeblego/)